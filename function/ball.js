const index = require("..");


const a = 4/3;
const phi = 3.14;

// function to calculate volume of ball
function volumeBall(a, phi, jariJari) {
  return a * phi * jariJari ** 3;
}

function inputBall () {
    index.rl.question (`Input Jari-Jari: `, (jariJari) => {
        if (!isNaN(jariJari) && !index.isEmptyOrSpaces(jariJari)) {
            console.log(`Ball's Volume Is: ${volumeBall(a, phi, jariJari)}\n`);
            index.rl.close();
        } else {
            console.log(`Jari-Jari Harus Berbentuk Angka!!!`);
            inputBall ();
        }
    });
}

module.exports = { inputBall };