/* If we use callback function, we can't add another logic outside the function */

// Import readline
// const readline = require('readline');
// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout,
// });

const index = require("..");

// Function to calculate Pyramid volume
function volumePyramid (s, h) {
  return 1/3 * s * s * h;
}

/* Alternative Way */
// All input just in one code
function inputPyramid() {
  index.rl.question('Side: ', function (s) {
    index.rl.question('Height: ', (h) => {
        if (s > 0 && h > 0) {
          console.log(`\nPyramid: ${volumePyramid(s, h)}`);
          index.rl.close();
        } else {
          console.log(`Side and Height must be a number\n`);
          inputPyramid();
      };
    });
  });
}
/* End Alternative Way */

// console.log(`Pyramid`);
// console.log(`=========`);
module.exports = { inputPyramid };// Call Alternative Way
