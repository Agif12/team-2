// Import readline
// const readline = require("readline");
// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout,
// });

const index = require("..");

const phi = 3.14;
// Function to calculate tube volume
function tubeVolume(radius, height, phi) {
  //Function dan 3 parameter
  return phi * radius * radius * height; //pengembalian nilai dengan argumen
}

/* Alternative Way */
// All input just in one code
function inputTube() {
  index.rl.question("Radius: ", function (radius) {
    //rl.question("Phi: ", (phi) => {
    index.rl.question("Height: ", (height) => {
      if (radius > 0 && phi > 0 && height > 0) {
        console.log(`\nTube: ${tubeVolume(radius, height, phi)}`);
        index.rl.close();
      } else {
        console.log(`Radius and Height must be a number\n`);
        inputTube();
      }
    });
  });
  //});
}
/* End Alternative Way */

// console.log(`Tube Volume`);
// console.log(`=========`);
// inputTube(); // Call Alternative function the Tube  Volume
module.exports = { inputTube };
