const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const ball = require("./function/ball");
const pyramid = require("./function/pyramid");
const tube = require("./function/tube");

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

function menu () {
  console.log(`Menu`);
  console.log(`=====`);
  console.log(`1. Ball`);
  console.log(`2. Pyramid`);
  console.log(`3. Tube`);
  console.log(`4. Exit`);

  rl.question (`Pilih Dong:`, (option) => {
      if (!isNaN(option)) {
        if (option == 1) {
          ball.inputBall();
        } else if (option == 2) {
          pyramid.inputPyramid();
        } else if (option == 3) {
          tube.inputTube();
        } else if (option == 4) {
          rl.close();
        }
      } else {
        console.log(`Option must be 1 to 4!\n`);
        menu();
      }
  });
}

menu ();

module.exports.rl = rl;
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
